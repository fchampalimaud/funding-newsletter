from setuptools import setup, find_packages

setup(
    name="funding-newsletter",
    version=0.1,
    description="""""",
    author=["Ricardo Ribeiro"],
    author_email=["ricardojvr@gmail.com"],
    url="https://bitbucket.org/fchampalimaud/research-core-funding-newsletter",
    packages=find_packages(),
    package_data={"funding_newsletter": ["templates/funding_newsletter/*.html"]},
)
